/*
    FreeRTOS V7.2.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!
    
    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    
    http://www.FreeRTOS.org - Documentation, training, latest information, 
    license and contact details.
    
    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell 
    the code with commercial support, indemnification, and middleware, under 
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under 
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

/*
 * This is a very simple demo that demonstrates task and queue usages only in
 * a simple and minimal FreeRTOS configuration.  Details of other FreeRTOS 
 * features (API functions, tracing features, diagnostic hook functions, memory
 * management, etc.) can be found on the FreeRTOS web site 
 * (http://www.FreeRTOS.org) and in the FreeRTOS book.
 *
 * Details of this demo (what it does, how it should behave, etc.) can be found
 * in the accompanying PDF application note.
 *
*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
/* Standard include. */
#include <stdio.h>

/* User Include. */
#include "testcase.h"

/* Priorities at which the tasks are created. */
#define mainQUEUE_RECEIVE_TASK_PRIORITY		( tskIDLE_PRIORITY + 2 )
#define	mainQUEUE_SEND_TASK_PRIORITY		( tskIDLE_PRIORITY + 1 )

/* The rate at which data is sent to the queue, specified in milliseconds. */
#define mainQUEUE_SEND_FREQUENCY_MS			( 10 / portTICK_RATE_MS )

/* The number of items the queue can hold.  This is 1 as the receive task
will remove items as they are added, meaning the send task should always find
the queue empty. */
#define mainQUEUE_LENGTH					( 1 )

/* The ITM port is used to direct the printf() output to the serial window in 
the Keil simulator IDE. */
#define mainITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define mainITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define mainDEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define mainTRCENA          0x01000000

/*-----------------------------------------------------------*/

/*
 * Redirects the printf() output to the serial window in the Keil simulator
 * IDE.
 */
int fputc( int iChar, FILE *pxNotUsed );

/*-----------------------------------------------------------*/

/* The queue used by both tasks. */
static xQueueHandle xQueue = NULL;
static xQueueHandle q1=NULL, q2=NULL, q3=NULL, qp=NULL;

/* One array position is used for each task created by this demo.  The 
variables in this array are set and cleared by the trace macros within
FreeRTOS, and displayed on the logic analyzer window within the Keil IDE -
the result of which being that the logic analyzer shows which task is
running when. */
unsigned long ulTaskNumber[ configEXPECTED_NO_RUNNING_TASKS ];

/*-----------------------------------------------------------*/

typedef struct task_data{
	unsigned int task_id;
	unsigned int data;
}task_data;

int main(void)
{
	/*
	 * Uncomment the question which you want to test 
	 * Make sure that only one test is uncommented. Anyway it will go to infinite loop. 
	 */
	//question1_test();
	//question2_test();
	//question3_test();
	//question4_test();
	question5_test();
}

/* Question 1: Test Case and Helper functions Start here */ 

static void question1_test(){
	// Solution To question 1
	printf("Program Started\n");
	/* Create the queue. */
	
	xQueue = xQueueCreate(mainQUEUE_LENGTH, sizeof( int ));
	q1 = xQueueCreate(3, sizeof(int));
	printf("Initialized q1\n");

	q2 = xQueueCreate(3, sizeof(int));
	printf("Initialized q2 \n");
	
	q3 = xQueueCreate(3, sizeof(int));
	printf("Initialized q3\n");
	
	qp = xQueueCreate(3, sizeof(task_data));
	printf("Initialized qp\n");
	

	//xTaskCreate(t1, ( signed char * ) "T1", configMINIMAL_STACK_SIZE, NULL, 2, NULL );
	xTaskCreate(t1, ( signed char * ) "T1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(t2, ( signed char * ) "T2", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(t3, ( signed char * ) "T3", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(tp, ( signed char * ) "TP", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
	// tp task has the lowest priority

	/* Start the tasks running. */
	printf("All queues created. Starting Scheduler\n");
	
	vTaskStartScheduler();

	printf("You reached here. Some error occured\n");;
	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
}

// Task T1 as asked in question 1
static void t1(void *pvParameters){
	int ulReceivedValue;
	task_data sendData ;
	unsigned int id;
	id = pxGetCurrentTaskNumber();
	for( ;; )
	{
		printf("Running Task 1\n");
		xQueueReceive( q1, &ulReceivedValue, portMAX_DELAY );
		printf("Task t1 received value %d\n", ulReceivedValue);
		sendData.task_id = id; // Adding id to be sent
		sendData.data = id * ulReceivedValue; // the count received is multiplied by its on task id
		xQueueSend( qp, &sendData, portMAX_DELAY);
	}
}

// Task T2 as asked in question 1
static void t2(void *pvParameters){
	int ulReceivedValue;
	task_data sendData;
	unsigned int id;
	id = pxGetCurrentTaskNumber();
	for( ;; )
	{
		printf("Running Task 2 \n");;
		xQueueReceive( q2, &ulReceivedValue, portMAX_DELAY );
		printf("Task t2 received value %d\n", ulReceivedValue);
		
		sendData.task_id = id; // Adding id to be sent
		sendData.data = id * ulReceivedValue; // the count received is multiplied by its on task id
		xQueueSend(qp, &sendData, 0);
	}
}

// Task T3 as asked in question 1
static void t3(void *pvParameters){
	int ulReceivedValue;
	task_data sendData;
	unsigned int id;
	id = pxGetCurrentTaskNumber();
	for( ;; )
	{
		printf("Running Task 3 \n");;
		xQueueReceive( q3, &ulReceivedValue, portMAX_DELAY );
		printf("Task t3 received value %d\n", ulReceivedValue);
		sendData.task_id = id; // Adding id to be sent
		sendData.data = id * ulReceivedValue; // the count received is multiplied by its on task id
		xQueueSend(qp, &sendData, 0);
	}
}

// Task TP as asked in question 1
static void tp(void *pvParameters){
	task_data ulReceivedValue;
	portTickType xNextWakeTime = xTaskGetTickCount();
	// Gets current tick of the CPU 
	// Will be used later on to increment
	unsigned int id;
	int count = 1;

	id = pxGetCurrentTaskNumber(); // Obtains id of the existing process
	for( ;; )
	{
		printf("Running Task P\n");;
		if(count%3==1){
			printf("Task P Sent Value: %d to Queue 1\n", count);
			if(!xQueueSend(q1, &count, 0))
				printf("Sending failed to Queue1 \n");
		}
		else if(count%3==2){
			printf("Task P Sent Value: %d to Queue 2\n", count);
			xQueueSend(q2, &count, 0);
		}
		else{
			printf("Task P Sent Value: %d to Queue 3\n", count);
			xQueueSend(q3, &count, 0);
		}
		
		if(xQueueReceive(qp, &ulReceivedValue, portMAX_DELAY)){
			printf("The Task ID %d has sent the value %d\n", ulReceivedValue.task_id, ulReceivedValue.data);
		}
		count ++;	
		vTaskDelayUntil(&xNextWakeTime, 1000 );
	}
}

/* Question 1: Test Case and Helper functions End here */ 


/* Question 2: Test Case and Helper Functions Start Here */
static void question2_test(){
	// Creating tasks p1 to p5 with decreasing priority.
	// P1 has highest priority and P5 has lowest priority.
	xTaskCreate(task_runner, ( signed char * ) "P1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+5, NULL );
	xTaskCreate(task_runner, ( signed char * ) "P2", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+4, NULL );
	xTaskCreate(task_runner, ( signed char * ) "P3", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+3, NULL );
	xTaskCreate(task_runner, ( signed char * ) "P4", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(task_runner, ( signed char * ) "P5", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
	// All required tasks created
	
	/* Start the tasks running. */
	printf("All queues created. Starting Scheduler\n");
	vTaskStartScheduler();
	printf("You reached here. Some error occured\n");;
	for( ;; );
}

void task_runner(void *pvParameters){
	/*
	 * I am using the same function to spawn all the tasks required 
	 * in question 2.
	 * I will create different tasks using different priorities in the question2_test()
	 * All those tasks will use the same function.
	 */
	
	portTickType xNextWakeTime = xTaskGetTickCount();
	unsigned int id, period;
	id = pxGetCurrentTaskNumber()+1; // Obtains id of the existing process
	period = id*1000; // Period for task id 1 is 1000ms. task id 2 = 2000ms
	for( ;; )
	{	printf("Hi, I have woken up. My ID is: %d\n", id);
		vTaskDelayUntil(&xNextWakeTime, period);
	}
}

/* Question 2: Test Case and Helper Functions End Here */

/* Question 3 Test Case and Helper functions Start Here */ 

static void question3_test(){
	/* 
	* This question uses a function: question3_queue_info(void)
  * which is defined in queue.c 
	 */
	q1 = xQueueCreateWrapper(5, sizeof(int));
	xTaskCreate(test_send_question3, ( signed char * ) "T1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(test_send_question3, ( signed char * ) "T2", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(test_receive_question3, ( signed char * ) "T3", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(test_receive_question3, ( signed char * ) "T4", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	// Created 4 tasks which wait on queue: q1 
	xTaskCreate(dummy_process_question3, ( signed char * ) "T5", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	// Running a dummy process which keeps printing the queue info as required in question 3 for testing 
	// This test case verifies the number of tasks waiting in the queue
	
	printf("All queues created. Starting Scheduler\n");
	
	vTaskStartScheduler();

	printf("If You reached here then Some error has occured\n");;
	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
}

// Function for testing question 3
void test_receive_question3(void *pvParameters){
	int ReceivedValue;
	for(;;){
		printf("Starting Receive\n");
		xQueueReceive(q1, &ReceivedValue, portMAX_DELAY);
		printf("Received Value: %d\n", ReceivedValue);
	}
}

void test_send_question3(void *pvParameters){
	int sendData ;
	sendData=100;
	for(;;){
		printf("Starting Send\n");
		xQueueSend(q1, &sendData, 0);
		vTaskDelay(10000);
	}
}


static void dummy_process_question3(void *pvParameters){
	/*
	 * This process prints the queue stats once every second. 
	 * In the main function or the function inside which you need to test the queue 
	 * Do this; 	xTaskCreate(dummy_process, ( signed char * ) "T1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	 */
	for(;;){
		printf("Starting dummy\n");
		question3_queue_info(q1);
		vTaskDelay(1000);
	}
}

/* Question 3 Test Case and Helper Functions End here */


/* Question 4 Test Case and Helper Functions Start Here */ 
static void question4_test(){
	/*
	 * This function covers the test cases required for testing question4 
	 * 
	 * Functions Defined to make this Question Work:
	 *  xQueueCreateWrapper -> points to xQueueGenericCreate in queue.c
	 * Right click and "Go to Definition of xQueueCreateWrapper" to see exact definition of function.
	 */
	q1 = xQueueCreateWrapper(5, sizeof(int));
	// Create a Queue Handle using the newly created function xQueueCreateWrapper
	
	xTaskCreate(test_send_question4, ( signed char * ) "T1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(test_send_question4, ( signed char * ) "T1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );
	xTaskCreate(test_receive_question4, ( signed char * ) "T1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+3, NULL );
	// Created 3 tasks. 2 to send data into queue and 1 to receive the data sent 
	
	/* Start the tasks running. */
	printf("Starting Scheduler\n");
	
	vTaskStartScheduler();
	printf("You reached here. Some error occured\n");;
	for( ;; );
}

void test_receive_question4(void *pvParameters){
	queueData ReceivedValue;
	for(;;){
		printf("Starting Receive\n");
		xQueueReceive(q1, &ReceivedValue, portMAX_DELAY);
		printf("Received From: Task ID: %d, Received value %d\n", ReceivedValue.task_id, ReceivedValue.data);
	}
}

void test_send_question4(void *pvParameters){
	int sendData ;
	sendData=100;
	for(;;){
		printf("Starting Send\n");
		xQueueSendWrapper(q1, &sendData, 0);
		// Rather than Sending Data through inbuild function
		// I am using my own xQueueSendWrapper function which is defined in queue.c file
		vTaskDelay(10000);
	}
}
/* Question 4 Test Case and Helper functions end Here */ 

/* Question 5 Test Case and Helper functions Start Here */
static void question5_test(){
	/*
	Test Straegy for Testing this. 
	Create a Mutex using xMutexCreate
	Create a task which obtains the mutex.
	
	Create 3 tasks of different probabilities. Show that using your function solves the problem. 
	*/
	q1 = xQueueCreateMutex('a');
	// Create a Queue Handle using the newly created function xQueueCreateWrapper
	
	xTaskCreate(lowest_priority_question5, ( signed char * ) "Lowest Priority", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
	xTaskCreate(highest_priority_question5, (signed char *) "Highest Priority", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+3, NULL );
	xTaskCreate(medium_priority_question5, (signed char *) "Medium Priority", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL);
	
	// Here we have created 3 tasks. One with highest priority. One with lowest and another with medium priority. 
	// We will simulate a case were priority inversion happens. 
	// How to simulate priority inversion: 
	// As soon as simulation starts, run the lowest priority task. As soon as it starts make it occupy a mutex
	// After occupying on the mutex and stars printing 1 to 1000. 
	// While it is printing from 1 to 100 the "highest priority" task comes in and tries to get the mutex but 
	// gets blocked since the mutex is already occupied by a process. While the highest priority task is blocked
	// Another medium priority task comes in and takes over the CPU. Now Medium priority task will complete its 
	// run time since high priority job is blocked. As soon as its done, low priority job will starts its execution
	// and release the mutex and then the high priority job will run. 
	// So we see that a medium priority job comes after a high priority job and still finishes first: That's a priority
	// inversion and should be prevented in real time systems. 
	
	// NOTE: Priority inversion will only be observed during the first periodic cycle of the three programs. 
	// Since from the next periodic cycle it may so happen that the high priority job may come first and take the CPU always. 
	
	
	/* Start the tasks running. */
	printf("Starting Scheduler\n");
	
	vTaskStartScheduler();
	printf("You reached here. Some error occured\n");;
	for( ;; );
}


static void lowest_priority_question5(){
	unsigned int count;
	for(;;){
		// This is the lowest priority job as mentioned in the question5_test() 
		// It tries to take the mutex as soon as it starts execution. 
		printf("Lowest priority job has started\n");
		xQueueTakeMutexRecursive( q1,  100);
		for(count=0;count<10;count++){
			printf("Lowest priority job printing : %d\n", count);
			vTaskDelay(10);
		}
		vTaskDelete(NULL);
	}
}

static void highest_priority_question5(){
	unsigned long receiveData;
	for(;;){
		vTaskDelay(70); // Delaying it so that it starts after low priroity job is done acquiring mutex
		printf("highest priority job started execution. \n");
		xQueueTakeMutexRecursive( q1,  100); 
		// Tries to get acquire mutex as soon as it starts running 
		printf("Highest Priority job has acquired the mutex and is very happy now\n");
		vTaskDelete(NULL); // Delete the task since our demonstaration is complete
	}
}

static void medium_priority_question5(){
	// This simplay runs a task which never ends and make sure that it's damn fast.
	unsigned int count;
	for(;;){
		vTaskDelay(70); // Delaying it so that it comes after high priority job
		printf("Medium priority job has started. It will run peacefully\n");
		for(count=0;count<10;count++){
			printf("Medium priority job printing : %d\n", count);
			vTaskDelay(1);
		}
		printf("Medium priority job has completed printing and is going to die\n");
		vTaskDelete(NULL); // Deleting the task since required demo is complete
	}
}

/* Question 5 Test Case and Helper functions End Here */



/*-----------------------------------------------------------*/

	
int fputc( int iChar, FILE *pxNotUsed ) 
{
	/* Just to avoid compiler warnings. */
	( void ) pxNotUsed;

	if( mainDEMCR & mainTRCENA ) 
	{
		while( mainITM_Port32( 0 ) == 0 );
		mainITM_Port8( 0 ) = iChar;
  	}

  	return( iChar );
}
