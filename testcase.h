/*
 * Author : Vikas Yadav 
 * Email: vikas.yadav@iiitb.org
 *
 * This file contains all the headers required for testing questions 1 to 5
 */
 
static void question1_test();
static void t1(void * pvParameters);
static void t2(void * pvParameters);
static void t3(void * pvParameters);
static void tp(void * pvParameters);

static void question2_test();
void task_runner(void *pvParameters);

static void question3_test();
void test_receive_question3(void *pvParameters);
void test_send_question3(void *pvParameters);
static void dummy_process_question3(void *pvParameters);
  
static void question4_test();
void test_receive_question4(void *pvParameters);
void test_send_question4(void *pvParameters);

static void question5_test();
static void lowest_priority_question5();
static void highest_priority_question5();
static void medium_priority_question5();

